# Realia rejsu morskiego - dla nieżeglarzy

Tekst ten powstał dla osób, które wybierają się na swój pierwszy rejs morski i nie do końca wiedzą, z czym "to się je". Zauważyłem, że osoby, które pierwszy raz pojawiają się na jachcie, często mają odmienną wizję tego, jak będą wyglądać ich wymarzone wakacje na morzu, od realiów tego pięknego sportu. Ludzie nasyceni filmami o pięknych dziewczynach opalających się na pokładzie wielkiego katamaranu w pięknym słońcu i bezchmurnym niebie mogą wprowadzić niepotrzebne konflikty wśród pozostałych uczestników rejsu, gdy rzeczywistość okaże się mniej filmowa. Dlatego przeczytaj uważnie, bo postaram się opisać, jak to wygląda i co powinieneś wiedzieć przed swoim pierwszym rejsem.

## Fakty i mity, które mogą cię zaskoczyć
 * Rejs morskim jachtem żaglowym to nie wakacje na luksusowym jachcie z własnym szoferem i kucharzem.
 * Jacht prowadzimy, wydając komendy/polecenia (np. "grot precz", "oddaj cumę rufową", ...) w trybie rozkazującym. Jeżeli jesteś osobą, która nie toleruje rozkazów, to trzeba przywyknąć. Nikt nie będzie do ciebie mówił: "Czy mógłbyś proszę wyjść na pokład i ściągnąć grota, nasz największy żagiel, ponieważ bardzo wieje i robi się niebezpiecznie?"
 * Czasem ktoś na ciebie może krzyknąć. Nie dlatego, że cię nie lubi, ale dlatego, że chce mieć pewność, że usłyszałeś/-aś polecenie.
 * Nigdy nie zakładaj, że choroba morska może cię nie dotyczyć.

## Słownictwo żeglarskie
Nie będę tutaj wyjaśniać całego słownictwa żeglarskiego. Wybrałem tylko kilka podstawowych nazw, które najczęściej dotyczą nowych osób na jachcie. Pozostałe zostawiam kursom.

* Dziób - przód łodzi
* Rufa - tył łodzi
* Burta lewa/prawa - krawędź lewa/prawa łodzi
* Grot - największy żagiel, przy łódce z jednym masztem ten bliżej rufy
* Fok - na łodzi z jednym masztem najczęściej przedni żagiel 
* Szot lewy/prawy foka - lina przyczepiona do dolnej krawędzi foka, biegnąca z lewej i prawej strony (przy burcie). Służy do przeciągania żagla na lewą lub prawą stronę.
* Fał - lina do podnoszenia żagla
* Kontrafał - lina do opuszczania żagla 
* Kambuz - kuchnia
* Koja - łóżko
* Kajuta - twoje 3/4 ściany na jachcie
* Cuma - lina, którą przywiązany jest jacht do nabrzeża (keji). Możecie spotkać się również z nazwami szpring czy brest, ale na tym etapie nie jest to potrzebne. 
* [Odbijacz](http://osprzetzeglarski.pl/img/cms/produkty/ochraniacz-dziobu-odbijacz.jpg) - kawałek miękkiej gumy z linką, wiszący na burtach lub rufie. Zabezpiecza jacht przed obijaniem się o inne łodzie lub nabrzeże.
* Muring - lina zaczepiona do dna, którą podnosimy i przywiązujemy (cumujemy) jacht.
* Knaga - element służący do mocowania lin na jachcie.

## Węzły
Nie będziemy tutaj rozważać sztuki wiązania węzłów. Moim zdaniem nowa osoba na jachcie powinna już znać niżej wymienione węzły, ponieważ na ich naukę na wodzie nie ma czasu:

* Wyblinkę
* Węzeł knagowy
* Węzeł ratowniczy wiązany nie na sobie (pętla)
* Sztukę buchtowania liny.

Filmik pomocniczy jest [tutaj](https://www.youtube.com/watch?v=5kYM10i8oBc).

## Demokracja 
Nie ma czegoś takiego jak **demokracja**. Jak już pisałem, kapitan jest osobą, która posiada władzę absolutną na łajbie. W związku z tym demokratyczny system podejmowania decyzji nie istnieje. Oczywiście, mądry kapitan często słucha głosów swojej załogi, a w szczególności oficerów, ale decyzje dalej podejmowane są autonomicznie przez jedną osobę - KAPITANA. Jeżeli jesteś osobą, która lubi przejmować inicjatywę w każdej możliwej sytuacji, która stara się rządzić, to muszę cię zmartwić. Nie porządzisz.

## Załoga
**Załoga** - to po prostu wszystkie osoby biorące udział w rejsie. Najważniejszymi osobami wśród załogi są:

* **Kapitan/skiper/szyper** - odpowiada za wszystko, co dzieje się na jednostce i podczas rejsu,
* **Oficerowie** - więcej info [tutaj](https://pl.wikipedia.org/wiki/Oficer_(%C5%BCeglarstwo)) - w zależności od liczby załogi. Najczęściej będzie max. 3 oficerów.

[Wachty](https://pl.wikipedia.org/wiki/Wachta) - jest to po prostu podział załogi na mniejsze grupy, które są potrzebne do prowadzenia jachtu, przygotowywania posiłków, prowadzenia nawigacji, etc. Sposób podziału oraz godziny pełnienia "dyżurów wachtowych" ustala kapitan. Jeżeli jedziesz na rejs z dziewczyną/chłopakiem, nie zakładaj, że będziecie w tej samej wachcie. To ustala kapitan, a jak już pisałem - nie ma tutaj demokracji. Ja ustalam wachty w taki sposób, aby kompetencje, wiedza i "siła" danej wachty były zbliżone do siebie. Dlatego podzielenie się swoją wiedzą, kompetencjami, uprawnieniami jest dla mnie dość ważne.

Wachtę można porównać do klasy w szkole, a harmonogram pełnienia wacht jest jak plan lekcji, rozpisany na cały rejs. Jeżeli wymarzyłeś sobie imprezy do 23:00, a potem smaczne spanie do 10:00 rano, to trzeba zweryfikować ten pogląd. Może się zdarzyć konieczność stania na deszczu o 03:00 nad ranem i trzymania jakiegoś szota.

## Przykładowy pierwszy dzień rejsu
* 12:00 - Przybycie do portu
* 12:00 - 13:00 - Kapitan z pierwszym oficerem odbierają jacht
* 13:00 - 15:00 - przygotowanie do rejsu, aprowizacja, wybór kajut, koi, rozmieszczenie prowiantu
* 15:00 - 17:00 - Szkolenie bezpieczeństwa i omówienie alarmów
* 17:00 - kapitan podejmuje decyzję o trasie i czy w ogóle wychodzić tego dnia z portu.

## Przykładowy dzień - v1 
* 00:00 - 04:00 - Wachta 1 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego
* 04:00 - 08:00 - Wachta 2 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego
* 08:00 - 12:00 - Wachta 3 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego. Równolegle wachta kambuzowa przygotowuje śniadanie
* 12:00 - 16:00 - Wachta 1 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego 
* 16:00 - 20:00 - Wachta 2 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego. Równolegle wachta kambuzowa przygotowuje obiad
* 20:00 - 23:59 - Wachta 3 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego. 

## Przykładowy dzień - v2
* 08:00 - 12:00 - Wachta 1 - siedzicie w porcie, śniadanie, zwiedzanie. 
* 12:00 - 14:00 - Wachta 2 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego.
* 14:00 - 16:00 - Wachta 3 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego.
* 16:00 - 20:00 - Wachta 1 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego.
* 20:00 - 21:00 - Wchodzicie do portu.
* 20:00 - 24:00 - Kolacja, zwiedzanie, gitarka i śpiew. 

## Przykładowy dzień - v3
* 08:00 - 12:00 - Wachta 2 - siedzicie w porcie, śniadanie, wyjście z portu.
* 12:00 - 14:00 - Wachta 3 - Wychodzicie z portu. Prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego.
* 14:00 - 16:00 - Wachta 1 - prowadzenie jachtu, nawigacja, obserwacja, prowadzenie dziennika i nasłuchu radiowego. Wachta kambuzowa przygotowuje obiad.
* 16:00 - 20:00 - Wachta 2 - Rzucenie kotwicy w zatoce. 
* 16:00 - 20:00 - Wachta 2 - Wachta kotwiczna
* 20:00 - 24:00 - Wachta 3 - Wachta kotwiczna
* 00:00 - 04:00 - Wachta 1 - Wachta kotwiczna
* 04:00 - 08:00 - Wachta 2 - Wachta kotwiczna

Jak widzicie, wszystko zależy od "starego" - kapitana.