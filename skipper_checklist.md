# REJS MORSKI - checklista skippera

## Przygotowania 
- Powinniśmy rozpocząć od odwiedzenia strony [Ministerstwa Spraw Zagranicznych](https://www.gov.pl/web/dyplomacja/informacje-dla-podrozujacych), gdzie znajdziemy informacje odnośnie kraju, do którego się wybieramy. Oczywiście, informacje te należy zweryfikować.
- Skompletowanie załogi oraz upewnienie się, że ludzie będą pozytywnie nastawieni do rejsu.
- Ułożenie "przewidywanej" trasy rejsu z datami i uzgodnienie jej z załogą - passage planning.
- Telefony i adresy uczestników rejsu oraz osób kontaktowych (np. w celu udzielenia informacji medycznych).
- Kontakt do najbliższej placówki dyplomatycznej.
- Dokumenty czarterowe oraz ustalenie wszelkich spraw z opłatami, ubezpieczeniem i przejęciem jachtu.
- Personal Location Form - jeżeli w danym kraju jest stosowany w czasach COVID.
- Przesłać do armatora listę załogi, jeżeli jest to wymagane.

### Dokumenty i papiery
- Umowa czarteru i dokumenty rejsu.
- Dowód osobisty lub paszport.
- Prawo jazdy.
- Patent (żeglarski i motorowodny) + SRC/LRC.
- Dziennik PZŻ.
- Karta rejsu PZŻ - podpisana przez armatora.
- Druki opinii dla załogi.
- EKUZ - Europejska Karta Ubezpieczenia Zdrowotnego.
- Ubezpieczenie skippera.
- Podręcznik żeglarstwa - ponieważ nikt nie wie wszystkiego.
- Ubezpieczenie załogi - jeżeli tego chcą.
- Wszystko wrzucone do teczki, a bardziej wrażliwe dokumenty do Aquapacka.

### Kasa
- Na paliwo, opłaty portowe, transport, kaucję w walucie kraju, do którego jedziemy - 2 oficer.
- Jedną z ciekawych opcji (aby uniknąć przewalutowania) jest Revolut albo [kantor Alior Banku](https://kantor.aliorbank.pl).

### Ubranie na złą pogodę - sztormowe:
- Bielizna termoaktywna.
- Kurtka lub sztormiak żeglarski (odporność na wodę i wiatr), najlepiej z dodatkowymi odblaskami w jaskrawych kolorach, podgumowane rękawy, dobrze dopasowany kaptur.
- Spodnie lub sztormiak (ochrona przed wodą i wiatrem) ze ściągaczami na nogawkach.
- Buty z miękką, jasną podeszwą, która nie porysuje pokładu, z dobrym bieżnikiem zapewniającym przyczepność na mokrej nawierzchni. Można rozważyć kalosze żeglarskie.
- Ciepła czapka + rękawice do szorowania lin.

### Ubranie zwykłe - ilość dobrana do czasu rejsu:
- Bielizna.
- Skarpety - w tym kilka sztuk ciepłych.
- Spodnie lub spodenki do śmigania po jachcie, codziennej egzystencji, gotowania, spania.
- Podkoszulki.
- Sweter / polar / golf.
- Buty - crocsy, klapki basenowe, trampki - oczywiście nie rysujące pokładu.
- Czapka z daszkiem.
- Rękawiczki.

### Sprzęt / gadżety 
- Puszka kibica - róg mgłowy.
- Pławka dymna.
- Rakieta spadochronowa.
- RTL/SDR + antena = AIS.
- Ładowarka USB samochodowa (gniazdo zapalniczki) + kable do swojego telefonu.
- Baterie.
- Latarka czołowa + 2 komplety baterii, idealnie jak będzie miała też czerwone światło.
- Latarka ręczna.
- Telefon komórkowy z działającym roamingiem.
- Radiotelefon z kanałami morskimi.
- Radiotelefon PMR do łączności bliskiego zasięgu - wolne pasmo.
- Gwizdek.
- Scyzoryk / multitool / nóż żeglarski.
- Lodówka turystyczna - jeżeli nie ma na jachcie.
- Lifelina.
- Krawaty / linki.
- Karabinki.
- Przejściówki do węża na wodę.
- Banderka polska.
- Rzutka.
- Alkomat.
- Szelki/kamizelka pneumatyczna + wąsy.
- Panel solarny + przedłużka.
- Powerbank.
- JBL / Gitara.
- Lampka USB.
- Przedłużacz prądowy.
- Złodziejka.
- Przejściówka prądowa między gniazdami różnego typu.
- Panel solarny z wyjściem USB.

### Nawigacja
- Navionics / OpenCPN + mapy.
- Mapy papierowe.
- Locja.
- Lornetka.
- Namiernik.
- Trójkąty nawigacyjne + ołówki + gumka + długopis.
- Przenośnik.
- Zaplanowanie trasy - passage planning.
- Zainstalowane w telefonie aplikacje przydatne w żegludze - AIS, GPS, Navionics, Locus Map + OpenSeaMaps, Żegluj, Weather Radar.

### Zakupy - najczęściej zapominane
- Woda.
- Ręczniki papierowe.
- Worki na śmieci - najlepiej z ściągaczem.
- Papier toaletowy.
- Myjka i płyn do mycia naczyń.

### Apteczka
- Własne lekarstwa.

### Skrzynka bosmańska / narzędzia
- Ducktape.
- Izolacja.
- Klucz rowerowy - mały i kompaktowy.
- Gumy montażowe.
- WD40.
- Trytki.

### Inne:
- Śpiwór + poduszka.
- Okulary przeciwsłoneczne.
- Krem UV.
- Igła + nitka.
- Buty do wody, jeżeli planujemy schodzić z łodzi "na dziko".
- JBL GO2 - głośnik.
- Śpiewnik.

## W porcie - przed rejsem
- Karta bezpieczeństwa jachtu.
- Checklista jachtu.
- Sprawdzić stan paliwa i dotankować.

- WOBBLE:
    - W = Water filter
    - O = Oil
    - B = Belt
    - B = Bilges
    - L = Levels
    - E = Engine/Exhaust
```
Water Filtet - Shut the seacock and then remove the filter and give it a good rinse under the tap and put back in - make sure the cap is securely fitted and you MUST remember to open the seacock. The reason for closing the seacock is to ensure the pump stays flooded thus preventing any unnecessary wear on the impellor.

Oil - Check level and keep topped up.  Keep a spare bottle of oil just in case.  Generally it is a good idea to keep the oil close to the top mark on the dip stick, however, do NOT overfill.

Belt - Check for damage and tension.  The belt drives both the fresh water pump and the alternator, any loosening of the belt will reduce the efficiency of the pump and may reduce the charging output from the alternator.

Bilges - Look for signs of leaks which may indicate something more serious.

Levels - Fresh water coolant level to be precise - ensure it is topped up and you have spare coolant on board.  The coolant is an antifreeze/corrosion inhibitor not just fresh water.  However, in an emergency fresh water could be used until you can top up with proper coolant fluid.

Engine/Exhaust - Before starting the engine check the propeller is clear and no stray lines are hanging overboard. Once started check there is a good flow of water from the exhaust (check for a minimum of 30 secs).  Allow the engine to warm up, so it "ticks over" smoothly before setting off.

Spares you should carry:

Fuel; Oil, Filters, Belts, Impellor, Suitable tools
```