# Odprawa przed rejsem - kapitańska
## 1. Ustalenie podziału kajut
Przedstaw załodze dostępne kajuty. Omów ewentualne preferencje lub potrzeby poszczególnych członków załogi (np. bliskość toalety, cisza).

## 2. Wachty
#### Hierarchia na jachcie

* Wyjaśnij strukturę dowodzenia na jachcie, zaznaczając, że kapitan ma decydujący głos. 
* Przedstaw wybranych oficerów oraz ich odpowiedzialności

#### Omówienie wacht nawigacyjnych i kambuzowych

* Wachty nawigacyjne:
  * Wyjaśnij zasady pełnienia wacht nawigacyjnych, które obejmują kontrolę kursu, monitorowanie otoczenia i nasłuch radiowy.
  * Podkreśl, jak ważne jest przekazywanie dokładnych informacji przy zmianie wachty, szczególnie podczas pływania nocnego.
  * Podkreśl konieczność raportowania wszelkich zmian w kursie, warunkach pogodowych, położeniu statków w pobliżu i stanie sprzętu nawigacyjnego.
  * Prace na pokładzie - nadwachta, podwachta - wyjaśnić

* Wachty kambuzowe - Omów harmonogram posiłków i zasady korzystania z kuchni.

* Sprzątanie rejonów - Wyjaśnij, jakie są oczekiwania co do utrzymania porządku i jak często należy sprzątać.


* Harmonogram wacht
  * Rozpisz harmonogram wacht na kartce i wywieś go w widocznym miejscu na jachcie.
  * Upewnij się, że każdy członek załogi zna swój czas pełnienia wachty i jest przygotowany do jej przekazania.

## 3. Obsługa toalet
* Wyjaśnij, jak należy używać pompy ręcznej, jeśli toalety są w nią wyposażone.
* Przypomnij, że do toalet nie należy wrzucać papieru toaletowego ani żadnych odpadów innych niż te naturalne.

## 4. Podstawowe węzły, komendy i manewry cumowania
* Węzły - Naucz załogę kilku podstawowych węzłów, takich jak węzeł ratowniczy, knagowy, oraz wyblinkę.
* Komendy - Omów najważniejsze komendy, które będą używane na jachcie.
* Manewry cumowania - Przedstaw najpopularniejsze manewry cumowania stosowane w danym kraju, w którym żeglujecie. Omów, jak przygotować cumy, odbijacze i inne elementy, aby manewr cumowania przebiegł sprawnie i bezpiecznie.

## 5. Bezpieczeństwo i alarmy
#### Alarm ogólny
* Wyjaśnij, kiedy i w jaki sposób uruchamia się alarm ogólny. 
* Podaj przykłady sytuacji, które mogą wymagać aktywacji alarmu ogólnego, takie jak nagła zmiana pogody, wykrycie obiektów w wodzie, problemy techniczne.

#### Alarm gazowy
* Omów procedurę działania w przypadku wycieku gazu na jachcie.
* Wyznacz osoby odpowiedzialne za zamknięcie zaworów gazowych, wywietrzenie przestrzeni i poinformowanie kapitana.
* Zadbaj, aby każdy członek załogi znał lokalizację zaworów i wiedział, jak je obsługiwać.

#### Alarm "opuścić statek"

* Przedstaw zasady postępowania w sytuacji alarmowej wymagającej opuszczenia statku.
* Podziel obowiązki pomiędzy członków załogi:
  * Kto odpowiada za tratwę ratunkową?
  * Kto zabiera grabbag (torbę ratunkową)?
  * Kto odpowiada za pirotechnikę (race, flary)?
  * Kto obsługuje urządzenia distress (telefon, radio)?
    
* Przeprowadź świczebnyh alarmo MOB, aby upewnić się, że każdy zna swoje zadania i wie, jak je wykonać w stresującej sytuacji.