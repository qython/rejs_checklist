# Linki

## Mapy
* [bbbike.org](https://extract.bbbike.org/) - na tej stronie wygenerujacie sobie pliki z OpenSeaMap na różne typy urządzeń. Przydatne nie tylko dla żeglarzy. 
* [OpenSeaMap FTP](https://ftp.gwdg.de/pub/misc/openstreetmap/openseamap/charts/) - OpenSeaMap repozytorium

## Meteo
* [Polska prognoza pogody - ICM](https://www.meteo.pl/mapy)
* [Polska prognoza pogody - IMGW](https://baltyk.imgw.pl/) - dla obszaru Morza Bałtyckiego.
* [Niemiecka prognoza pogody - wetter3](https://www.wetter3.de)
* [Chorwacka prognoza pogody](https://meteo.hr/prognoze_e.php?section=prognoze_model&param=ala_k&el=web_uv10_HRv8_)
* [Włoska prognoza pogody](https://www.lamma.toscana.it/en/sea/wind-sea.php) - W małej rozdzielczości dla całego Morza Śródziemnego.
* [Ocean prediction center](https://ocean.weather.gov/) - Prognoza pogody dla 

## Różne
* [Locja bałtyku + mapy + publikacje nautyczne](https://wojtekbartoszynski.pl/index.html) - link do strony Wojciecha Bartoszyńskiego
* [Mapa wypadków morskich](https://www.google.com/maps/d/u/0/viewer?ll=3.977504674312716%2C0&z=2&mid=1-C0cVtJ9c23GD0RhcPw9VrBE2l_N0CWc) - ponownie opracowanie Wojtka.