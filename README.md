# Rejsowy niezbędnik

To repozytorium powstało, aby ułatwić szyprowi oraz załodze przygotowania do rekreacyjnego rejsu jachtem żaglowym. Możecie z niego korzystać do woli, zgłaszać uwagi pod tym adresem [jsm@pyton.systems](mailto:jsm@pyton.systems), lub jeśli potraficie pracować z GITem, to po prostu wystawić pull requesta. Znaczna część rzeczy wpisanych tutaj to moje prywatne zbiory lub takie, które są publicznie dostępne.

Załodze rozsyłam najczęściej tylko linki "Realia rejsu morskiego" oraz "Checklistę załoganta". Oczywiście, jeśli kogoś interesuje całość, to śmiało.

Pamiętaj, że nie jestem pisarzem czy poetą, tylko gościem z IT. Moja polszczyzna i błędy mogą razić oczy.

## Co tutaj znajdziecie
#### Przydatne linki
* Znajdziecie [tutaj](./links.md)

#### Przygotowania do rejsu - przeczytać
* [Realia rejsu morskiego](./realia_rejsu_morskiego.md) - Dokument mający na celu pokazanie, jak wygląda przykładowy rejs. Polecam dla osób, które nie żeglowały wcześniej.
* [Checklista załoganta](./zaloga_checklist.md) - Co zabrać, jak się spakować.
* [Checklista skippera](./skipper_checklist.md) - Dokument przeznaczony dla kapitanów przygotowujących się do rejsu.
* [Kilka słów o chorobie morskiej](http://www.seamaster.pl/pdf/choroba-morska.pdf) - Świetny dokument o chorobie morskiej opracowany przez Andrzeja Pochodaja.

#### Przygotowania do rejsu - wydrukować
* [Druk - Opinia z rejsu](./dokumenty_do_pobrania/opinia_z_rejsu.pdf) - Obowiązujący dokument PZŻ potwierdzający staż uczestnika rejsu. Otrzymuje uczestnik rejsu, wypełnia kapitan.
* [Druk - Karta rejsu](./dokumenty_do_pobrania/karta_rejsu.pdf) - Obowiązujący dokument PZŻ potwierdzający staż prowadzącego rejs. Otrzymuje kapitan, wypełnia armator/właściciel jachtu.
* Druk - dziennik - w przygotowaniu.

#### Na rejsie
* [Odprawa bezpieczeństwa](./odprawa_przed_rejsem.md) - Dokument, według którego przeprowadzam odprawę bezpieczeństwa - *W przygotowaniu*.
* [Wypływamy checklista](./wyplywamy_checklist.md) - Lista czynności do sprawdzenia zawsze przed wyjściem w morze. Jeden z najczęściej używanych dokumentów. Wieszam go na ścianie w mesie i przed każdym wyjściem sprawdzam dokładnie.

#### Po rejsie
* [Skipper checklista porejsowa](./skipper_checklist_po_rejsie.md) - Czynności do wykonania po zakończeniu rajsu jeszcze. Przeczytać jeszcze w porcie końcowym!

## Osoby tworzące to repozytorium

- [Piotr Pyciński](https://www.pyton.systems) - [jsm\@pyton.systems](mailto:jsm@pyton.systems)
- Łukasz Proszek
- Grzegorz Gielas
- Dopisywać się proszę

## Licencja
![CC](./images/cc_licencja.png) [Creative Commons 4.0](https://creativecommons.pl/poznaj-licencje-creative-commons/) 
- Uznanie autorstwa
- Użycie niekomercyjne
- Na tych samych warunkach 4.0

Licencja ta pozwala na rozpowszechnianie, przedstawianie i wykonywanie utworu jedynie w celach niekomercyjnych oraz tak długo, jak utwory zależne będą również obejmowane tą samą licencją.