# Skipper checklist - po rejsie

- Wypełnić u armatora [kartę resju](./dokumenty_do_pobrania/karta_rejsu.pdf)
- Wypełnić [opinię z rejsu](./dokumenty_do_pobrania/opinia_z_rejsu.pdf) dla każdego załoganta. Jeżeli się należy. Pamiętajcie że nie ma obowiązku wypełniania opinii, ale jest to dobra praktyka.
