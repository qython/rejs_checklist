# REJS MORSKI - checklista załoganta

## Dokumenty obowiązkowe
* Dowód osobisty lub paszport
* EKUZ - Europejska Karta Ubezpieczenia Zdrowotnego - wydawana praktycznie od ręki w **normalnych** placówkach NFZ. Więcej informacji [tutaj](https://www.nfz.gov.pl/dla-pacjenta/nasze-zdrowie-w-ue/leczenie-w-krajach-unii-europejskiej-i-efta/jak-wyrobic-karte-ekuz/).
* W czasach COVID - paszport COVID-owy cyfrowy (mObywatel) i/lub papierowy (wydrukowany), certyfikat szczepienia, wynik testu lub jakikolwiek dokument, który pozwala bezproblemowo przekraczać granice. Więcej informacji na stronie [MSZ](https://www.gov.pl/web/dyplomacja/informacje-dla-podrozujacych).

## Dokumenty opcjonalne - jeżeli posiadasz
* Prawo jazdy
* Patenty (żeglarskie i/lub motorowodne)
* Książeczka żeglarska
* Certyfikat operatora łączności (SRC, G1E, G2E, GOC, LRC, VHF)
* Ubezpieczenie indywidualne

## Kasa
* Drobne (monety) na toalety na trasie – bywają płatne, a nie wszędzie można płacić kartą.
* Lokalna waluta kraju docelowego
* Karta płatnicza. Jedną z ciekawych opcji (aby uniknąć przewalutowania) jest [Revolut](https://www.revolut.com/en-PL), [Kantor Walutowy](https://kantor.aliorbank.pl), ZEN, Curve.

## Co spakować
> Jeżeli jest to Twój pierwszy rejs i od czasu do czasu śmigasz po górach, to najprawdopodobniej 90% wyposażenia już masz. Zdecydowanie nie namawiam do kupowania profesjonalnych ubrań i sprzętu żeglarskiego na pierwszy rejs. Idea jest prosta: zwykłe ubrania + coś nieprzemakalnego i ciepłego. Profesjonalne sztormiaki, szeklownik, namierniki, obuwie żeglarskie zostawiamy na późniejsze rejsy, jak złapiecie zajawkę.

- **Torba sportowa / worek żeglarski / miękki plecak.** Torba powinna być miękka, najlepiej z wodoodpornego lub wodoszczelnego materiału. Nie zabieramy popularnych twardych walizek z kółkami, uwielbianych w podróżach samolotem, ponieważ na jachcie mogą po prostu nie zmieścić się w bakistach. Można pokusić się o wrzucenie kilku rzeczy dodatkowo do worka z folii PET, aby mieć pewność, że nie przemokną. Na pierwszy rejs zwykła torba sportowa jest jak najbardziej OK.

- **Ubranie na złą pogodę - sztormiak / kurtka nieprzemakalna / odzież BHP / spodnie.** Idealnie, aby odzież miała przyzwoitą odporność na deszcz i wiatr. Pamiętajmy, że organizm przy silnym wietrze, nawet bez deszczu, też dostanie w kość. Ja osobiście preferuję lekkie wodoodporne kurtki + softshell/polar z windstopperem pod spodem oraz spodnie nieprzemakalne. Oczywiście ilość poniższych dobieramy do długości rejsu, ale najczęściej wystarcza jeden komplet. Gdy już bawimy się profesjonalnie, to czasem możemy spotkać sztormiaki all-in-one (kurtka + spodnie). Gdy już będziecie profesjonalnie pływać, warto zainwestować w sztormiak z odblaskami w jaskrawych kolorach, z podgumowanymi rękawami i dobrze dopasowanym kapturem. Na pierwszy rejs weźcie po prostu to, co macie nieprzemakalnego, bez udziwnień.

- **Bielizna** - według własnego uznania. Jeśli bawimy się w to bardziej profesjonalnie, warto rozważyć bieliznę termoaktywną, z wełny merynosa czy odzież techniczną.

- **Buty** z miękką jasną podeszwą, która nie porysuje pokładu, z dobrym bieżnikiem zapewniającym przyczepność na mokrej nawierzchni. Można rozważyć kalosze żeglarskie. Unikamy podeszwy z ciemnej gumy, bo może zostawiać rysy/smugi na laminatowym pokładzie, a nie chcemy tego szorować. Buty na wycieczki w góry tutaj się nie sprawdzą.

- **Ciepła czapka.** Jeśli bawimy się profesjonalnie, warto, aby była z windstopperem.

- **Czapka z daszkiem** lub coś, co chroni głowę przed słońcem.

- **Rękawice do szorowania lin.** Chyba jeden z bardziej obowiązkowych sprzętów. Tutaj zostawiam dowolność, czy to będą robocze rękawice gumowe, rowerowe czy techniczne. Prywatnie używam rękawic ze sklepu BHP z gumą w części śródręcza. Rękawice mają być takie, że jak przyłożycie do nich bardzo tępy nóż, to nie przetnie Wam ręki. Proszę nie bagatelizować sprawy rękawic – trzeba je mieć, i to co najmniej 2-3 pary. Muring (na stałe zamocowana, tonąca lina, która zastępuje kotwicę) cały czas leży w wodzie i jest porośnięty glonami, skorupiakami i innym tałatajstwem. Te muszelki i inne żyjątka leżakujące na muringu są przyczyną bolesnych ran ciętych na rękach, jeśli złapiecie linę bez rękawic. Również podczas pracy z szotami (liny od żagli) można się nabawić oparzeń. Jak ktoś chce spróbować na sucho, to polecam założyć papier ścierny na wiertarkę, przyłożyć rękę i odpalić – efekt będzie podobny.

- **Spodnie lub spodenki** do śmigania po jachcie, codziennej egzystencji, gotowania, spania.
- **Podkoszulki**
- **Sweter / polar / golf**
- **Buty** - crocsy, klapki basenowe, trampki, adidasy
- **Kąpielówki / strój kąpielowy**
- **Ręcznik** (minimum 2, idealnie, aby były szybkoschnące)
- **Pasek do spodni**
- **Komin lub chusta**

## Inne:
- Śpiwór + poduszka (nie na każdym jachcie jest pościel)
- Okulary przeciwsłoneczne z polaryzacją
- Krem UV (zaczynamy od SPF 50 i schodzimy w dół)
- Buty do wody lub pod prysznic
- Kubek termiczny / własny kubek

## Sprzęt i gadżety 
> Należy pamiętać, że na jachcie mamy dostęp do prądu 12V (gniazda zapalniczki, czasem USB). 230V mamy podczas postoju w porcie. Wiem, są inwertery i inne patenty, ale to nie wykład z elektroniki jachtowej. Przy długich przelotach nawet prąd 12V może być reglamentowany.

- Ładowarka do elektroniki osobistej (gniazdo zapalniczki na 12V + 230V + kable)
- Powerbank (uwaga, w samolotach przy pojemności > 100 Wh lub 20000 mAh)
- Telefon komórkowy z działającym roamingiem
- Latarka - najlepiej czołówka + 2 komplety baterii, idealnie, jeśli będzie miała też czerwone światło

## Kosmetyki i lekarstwa
> Jachty posiadają na wyposażeniu apteczkę, ale znajdują się w niej głównie plastry i bandaże. Własne lekarstwa oraz choroby, które mogą być problematyczne podczas rejsu, należy zgłosić przed wypłynięciem kapitanowi. Odnośnie choroby morskiej nie będę pisał – bardzo dobrze temat został omówiony w [tym artykule](http://www.seamaster.pl/pdf/choroba-morska.pdf).

- Własne lekarstwa
- Mydło
- Szczoteczka do zębów
- Pasta do zębów
- Chusteczki
- Chusteczki mokre
- Worki na śmieci
- Maść na ukąszenia, jeżeli uprawiamy szuwarowo-bagienne pływanie na Mazurach
- Podpaski, tampony
