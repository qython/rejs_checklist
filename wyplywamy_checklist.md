# Checklista przed wypłynięciem

### Przygotowanie jachtu - zejklar
- Bandera na maszcie?
- Czy mamy odpowiednio dużo wody?
- Czy mamy odpowiednio dużo paliwa?
- Czy mamy odpowiednio dużo prowiantu?
- Odłączyć prąd,
- Zakręcić gaz,
- Sprawdzić stan akumulatorów,
- Zamknąć:
    - bulaje,
    - świetliki (skylight),
    - klapy,
    - luki,
    - kingstony,
- Klar w kambuzie/kubryku,
- Czy działa oświetlenie nawigacyjne?

### Przygotowanie kapitana
- Czy mam dokumenty jachtu (drugi oficer),
- Pobrać pogodę, griby,
- Przeglądnąć mapy oraz ułożyć je w kolejności,
- Przeliczyć załogę.

### Wyjście
- Opowiedzieć, dokąd płyniemy - przygotować załogę,
- Odpalić silnik, aby złapał temperaturę,
- Uruchomić przyrządy nawigacyjne,
- Przygotować bosak i ewentualne odbijacze manewrowe,
- Omówić manewr odejścia,
- Omówić manewr zapasowy,
- Wyznaczyć stanowiska,
- Zgłosić na VHF wyjście.

----

### Co kilka dni żeglugi
- WOBBLE - sprawdzić silnik co kilka dni żeglugi,
- Kontrola zęzy (chyba że przywalono w coś - wtedy częściej),
- Mocowanie pontonu,
- Sprawdzić stan lifeliny oraz relingów,
- Kontrola żagli, takielunku, lin,
- Mycie pokładu.